export LC_ALL=C

# number of days full backups will be kept
days_of_backups=3

# Unix userid performing the backup
backup_owner="backup"

# Password to encrypt backup files
encryption_password_file="${basedir}/encpwd.txt"

# directory to store backups
backupdir=/data/backups/mariadb

# Incrmental backups always based on full backup
#   cascading_incremental=true will use older incrementals to use less disk space
#   cascading_incremental=false will create incremental backup always based on the last full backup^
cascading_incremental=false

# Mysql username of mysqluser
defaults_file="/etc/mysql/backup.cnf"

# Parallel
processors="$(nproc --all)"

# path name of lock file to avoid parallel runs of the scripts
lockfile=/var/lock/mb_tools.lock

# not yet implemented, may cause issues
extra_backup_args=""


