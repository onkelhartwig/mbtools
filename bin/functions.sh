# Print message
msg() {
echo $(date -Iseconds) "$@"
}

# print message and exit with rc=3
error() {
   msg "$@"
   exit 3
}

# check content of configuratíon file
check_config() {
# source the config file
[ -e "${config}" ] && . "${config}" || "${basedir}/${config}"
# check valid entries in config file
[ "${days_of_backups}" -eq "${days_of_backups}" ] 2>/dev/null \
   || error days_of_backups ${days_of_backups} is not numeric
[ -d "${backupdir}" ] 2>&1 \
   || error backupdir "${backupdir}" does not exist
[ -w "${backupdir}" ] 2>&1 \
   || error no write access to backupdir "${backupdir}"
[ -r "${defaults_file}" ]  2>&1 \
   || error defaults_file ${defaults_file} can\'t be read
[ "$(id -un)" = "${backup_owner}" ] \
   || error script must run with user "backup_owner", current user is "$(id -un)"
[ "${cascading_incremental}" = false -o "${cascading_incremental}" = true ] \
   || error cascading_incremental is "${cascading_incremental}", must be false or true
}


create_incr_script() {
# create script to perform incremental backup
cat <<EOF >${tmpdir}/incr_backup.sh
#!/bin/bash
# Take incr backup
mariabackup \
   --defaults-extra-file="${defaults_file}" \
   --backup \
   --incremental-basedir="${full_dir}" \
   --target-dir="${incr_dir}"
EOF
chmod u+x ${tmpdir}/incr_backup.sh
}

create_full_script() {
# create script to perform full backup
cat <<EOF >${tmpdir}/full_backup.sh
#!/bin/bash
# Take full backup
mariabackup \
   --defaults-extra-file="${defaults_file}" \
   --backup \
   --target-dir="${full_dir}"
EOF
chmod u+x ${tmpdir}/full_backup.sh
}

create_prepare_script() {
# mariabackup --prepare --target-dir=/data/backups/mariadb/2023-06-14/restore --incremental-dir=/data/backups/mariadb/2023-06-14/incr-14-50
[ "${fulldir}" = "${fromdir}" ] && tmpinc="" || tmpinc='--incremental-dir="'${fromdir}'"'
cat <<EOF >${tmpdir}/prepare.sh
#!/bin/bash
# Take prepare backup
mariabackup \
   --defaults-extra-file="${defaults_file}" \
   --prepare  \
   --target-dir="${prepdir}"
EOF
chmod u+x ${tmpdir}/prepare.sh
[ "${fulldir}" = "${fromdir}" ] && return
cat <<EOF >>${tmpdir}/prepare.sh
mariabackup \
   --defaults-extra-file="${defaults_file}" \
   --prepare  \
   --target-dir="${prepdir}" \
   --incremental-dir="${fromdir}"
EOF

}

create_restore_script() {
# mariabackup --copy-back --target-dir=/data/backups/mariadb/2023-06-15/rest-15-42 --datadir=/data/backups/mariadb/2023-06-15/new-15-42
cat <<EOF >${tmpdir}/restore.sh
#!/bin/bash
# restore from prepared directory to new directory
mariabackup \
   --defaults-extra-file="${defaults_file}" \
   --copy-back --target-dir="${fromdir}" \
   --datadir="${restdir}"
EOF
chmod u+x ${tmpdir}/restore.sh
}

aquire_lock() {
# Establish a lock to avoid parallel run of mbtool scripts
exec 9<>"${lockfile}"
flock -n 9 && echo $$ >&9 || error can\'t establish lock on ${lockfile}
}

