#!/bin/bash

# establish base directory for the scripts
basedir=$( dirname "$( readlink -e "$0" )" )
toolname=$( basename "$0" )
cd "${basedir}"

# Print usage information
usage() {
cat <<EOF
Usage: ${toolname} [--config | -c configfile] 
                  --from | -f fromdir
                  [ -d | --debug ]
          --config configfile is located in ${basedir}/etc/config.sh by default
             must be an absolute path or relative to ${basedir}
          --from fromdir points to the directory prepared for the restore
             must be relative to ${backupdir} e.g. 2023-06-14/incr-15-40
          --debug enable debug mode, temporary files will not be deleted.

${toolname} creates a new directory below ${backudir} to replace @@datadir e.g. 2023-06-14/rest-15-40
           After ${toolname} has finished, the following tasks will replace the current mariadb datadir.
           These steps should be performed as root user
           1. stop Mariadb
              systemctl stop mariadb
           2. move away current datadir
              mv /var/lib/mysql /var/lib/mysql.save
           3. copy the restored datadir to /var/lib/mysql
              cp -arp <restored directory> /var/lib/mysql 
           4. set correct permissions in new datadir
              chown -R mysql:mysql /var/lib/mysql
              find /var/lib/mysql -type d -exec chmod g+rx {} \;
           5. start Mariadb
              systemctl start mariadb
           6. perform all necessary testing on you restore MariaDB
           7. Do not forget to delete /var/lib/mysql.save as soon you performed all checks
              against the new /var/lib/mysql 
EOF
exit 2
}

# set default config file
config="${basedir}/etc/config.sh"
debug=false

# parse arguments
SHORT=c:,f:,h,d
LONG=config:,from:,help,debug
OPTS=$( getopt -a -n ${toolname} --options ${SHORT} --longoptions ${LONG} -- "$@" )
[ $? -ne 0 ] && usage
eval set -- "${OPTS}"
while :
do
   case "$1" in
      -c | --config )
         config="$2"
         shift 2
         ;;
      -d | --debug )
         debug=true
         shift 1
         ;;
      -f | --from )
         fromdir="$2"
         shift 2
         ;;
      -h | --help)
         usage
         exit 2
         ;;
      --)
         shift;
         break
         ;;
      *)
         echo "Unexpected option: $1"
         usage
         ;;
   esac
done

if [ ! -e "${config}" -a ! -e "${basedir}/${config}" ]
then
   echo configfile ${config} not foud
   usage
fi

. "${basedir}"/bin/functions.sh
check_config
aquire_lock

# check required parameters

# fromdir must be given
[ -z "${fromdir}" ] && error required parameter --from not specified. you can check the available backup by issuing \( cd ${backupdir} \&\& ls -1d ????-??-??/prep-* \)
fromdir="${backupdir}/${fromdir}"

# check for required directories
# "${fromdir}" must exist
[ -d "${fromdir}" ] || error backup ${fromdir} not found in ${backupdir}

msg Starting restore using configfile ${config}

# create directory to create the restore files. Creation is done via hard links as mariabackup creates new inodes while preparingÂ
timestamp=$( awk -F '-' '{print $(NF-1)"-"$NF}' <<<"${fromdir}" )
restdir="$( dirname "${fromdir}")/rest-${timestamp}" 
[ ${debug} = true ] && echo ${fromdir}
[ ${debug} = true ] && echo ${restdir}
[ -e "${restdir}" ] && error the directory "${restdir}" already exists. pls carefully delete before restoring
# mariabackup --copy-back will create the directory ${restdir}

# create temporary directory to store files
tmpdir="$(mktemp -d ${TMP:-"${basedir}"/tmp}/restore.XXXXXXXX)"
chmod g+rwxs "${tmpdir}"

# create restore script
create_restore_script

# execute the script
msg executing "${tmpdir}/restdir.sh"
cat "${tmpdir}/restore.sh"
"${tmpdir}/restore.sh" 2>&1 | tee "${restdir}/restore.log"
RC=${PIPESTATUS[0]}
msg Returncode of "${tmpdir}/restore.sh" was ${RC}
if [ "${RC}" = 0 ]
then
   :
   msg Restore processing was successfull. Pls. perform the ROOT ACTIVITIES as
   msg described in restore.sh --help
else
   # delete the directory if the copy-back failed
   [ ${debug} = true ] && msg DEBUG is enabled. you need to manually remove "${restdir}" to avoid confusion || rm -rf "${restdir}"
fi
if [ ${debug} = false ]
then
   rm -rf "${tmpdir}"
fi
exit ${RC}

