#!/bin/bash

# establish base directory for the scripts
basedir=$( dirname "$( readlink -e "$0" )" )
toolname=$( basename "$0" )
cd "${basedir}"

# Print usage information
usage() {
cat <<EOF
Usage: ${toolname} [--config | -c configfile] 
                  --from | -f backupdir
          configfile is located in ${basedir}/etc/config.sh by default
             must be an absolute path or relative to ${basedir}
          backupdir points to the directory of the most recent backup you want to prepare
             must be relative to ${basedir} e.g. 2023-06-14/incr-15-40
${toolname} creates a directory ready to be restored to your MariaDB datadir
EOF
exit 2
}

# set default config file
config="${basedir}/etc/config.sh"
debug=false

# parse arguments
SHORT=c:,f:,h,d
LONG=config:,from:,help,debug
OPTS=$( getopt -a -n ${toolname} --options ${SHORT} --longoptions ${LONG} -- "$@" )
[ $? -ne 0 ] && usage
eval set -- "${OPTS}"
while :
do
   case "$1" in
      -c | --config )
         config="$2"
         shift 2
         ;;
      -d | --debug )
         debug=true
         shift 1
         ;;
      -f | --from )
         fromdir="$2"
         shift 2
         ;;
      -h | --help)
         usage
         exit 2
         ;;
      --)
         shift;
         break
         ;;
      *)
         echo "Unexpected option: $1"
         usage
         ;;
   esac
done

if [ ! -e "${config}" -a ! -e "${basedir}/${config}" ]
then
   echo configfile ${config} not foud
   usage
fi

. "${basedir}"/bin/functions.sh
check_config
aquire_lock

# check required parameters

# fromdir must be given
[ -z "${fromdir}" ] && error required parameter --from not specified. you can check the available backup by issuing \( cd ${backupdir} \&\& ls -1d ????-??-??/* \)
fromdir="${backupdir}/${fromdir}"

# check for required directories
# "${fromdir}" must exist
[ -d "${fromdir}" ] || error backup ${fromdir} not found in ${backupdir}
# the parent directory if "${fromdir}" must contain a full backup
fulldir=$(ls -d $(dirname "${fromdir}")/full-* 2>&1)
[ -d "${fulldir}" ] || error no full backup found at $(dirname "${fromdir}")

msg Starting prepare using configfile ${config}

# create directory to create the restore files. Creation is done via hard links as mariabackup creates new inodes while preparingÂ
timestamp=$( awk -F '-' '{print $(NF-1)"-"$NF}' <<<"${fromdir}" )
prepdir="$( dirname "${fromdir}")/prep-${timestamp}" 
[ ${debug} = true ] && echo ${fromdir}
[ ${debug} = true ] && echo ${fulldir}
[ ${debug} = true ] && echo ${prepdir}
mkdir "${prepdir}" || error the directory "${prepdir}" already exists. pls carefully delete before preparing

# delete the directory an recreate by copying the full backup
rmdir "${prepdir}" && cp -alpr "${fulldir}" "${prepdir}" || error Could not copy ${fulldir} to ${prepdir}

# create temporary directory to store files
tmpdir="$(mktemp -d ${TMP:-"${basedir}"/tmp}/prepare.XXXXXXXX)"
chmod g+rwxs "${tmpdir}"

# create prepare script
create_prepare_script

# execute the script
msg executing "${tmpdir}/prepdir.sh"
cat "${tmpdir}/prepare.sh"
"${tmpdir}/prepare.sh" 2>&1 | tee "${prepdir}/prepare.log"
RC=${PIPESTATUS[0]}
msg Returncode of "${tmpdir}/prepare.sh" was ${RC}
if [ "${RC}" = 0 ]
then
   :
else
   # delete the directory if the prepare failed
   [ ${debug} = true ] && msg DEBUG is enabled. you need to manually remove "${prepdir}" to avoid confusion || rm -rf "${prepdir}"
fi
if [ ${debug} = false ]
then
   rm -rf "${tmpdir}"
fi
exit ${RC}

