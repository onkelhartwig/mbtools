#!/bin/bash

# establish base directory for the scripts
basedir=$( dirname "$( readlink -e "$0" )" )
toolname=$( basename "$0" )
cd "${basedir}"

# Print usage information
usage() {
cat <<EOF
Usage: ${toolname} [--config\|-c configfile] [ --debug | -d ] 
          -c specfies the configfile located in ${basedir}/etc/config.sh by default
             configfile must be an absolute path or relative to ${basedir}
          -d enable debug mode, temporary files will not be deleted.
${toolname} creates a backup of your mariadb. 
   The design is to create one FULL backup once a day and continue with
   INCREMENTAL backups on that day. As soon the day changes a new FULL
   backup will be created.
EOF
exit 2
}

# set default config file
config="${basedir}/etc/config.sh"
debug=false

# parse arguments
SHORT=c:,d,h
LONG=config:,debug,help
OPTS=$( getopt -a -n "${toolname}" --options ${SHORT} --longoptions ${LONG} -- "$@" )
[ $? -ne 0 ] && usage
eval set -- "${OPTS}"
while :
do
   case "$1" in
      -c | --config )
         config="$2"
         shift 2
         ;;
      -d | --debug )
         debug=true
         shift 1
         ;;
      -h | --help)
         usage
         ;;
      --)
         shift;
         break
         ;;
      *)
         echo "Unexpected option: $1"
         usage
         ;;
   esac
done

# check existence of config file
if [ ! -e "${config}" -a ! -e "${basedir}/${config}" ]
then
   echo configfile ${config} not foud
   usage
   exit 2
fi

# source function library
. "${basedir}"/bin/functions.sh

# check consistence of config file
check_config
aquire_lock

msg Starting backup using configfile ${config}

# create directory to keep backup
today_dir="${backupdir}/$(date -Idate)"
incr_dir="${today_dir}/incr-$(date +%H-%M)"
full_dir="${today_dir}/full-$(date +%H-%M)"
# backup will be full if ${today_dir} does not exist, incremental if ${today_dir} already exists
mkdir "${today_dir}" && backup_type=full || backup_type=incr
if [ "${backup_type}" = "full" ]
then
   # create directory to keep the backup
   mkdir ${full_dir} || error directory ${full_dir} can\'t be created
   bkupdir="${full_dir}"
   # create temporary directory to store files
   tmpdir="$(mktemp -d ${TMP:-"${basedir}"/tmp}/backupfull.XXXXXXXX)"
   chmod g+rwxs "${tmpdir}"
   # create the full backup script
   create_full_script
   # execute the script
   msg executing "${tmpdir}/full_backup.sh"
   cat "${tmpdir}/full_backup.sh"
   "${tmpdir}/full_backup.sh" 2>&1 | tee "${full_dir}/full_backup.log"
   RC=${PIPESTATUS[0]}
   msg Returncode of "${tmpdir}/full_backup.sh" was ${RC}
   if [ "${RC}" = 0 ]
   then 
      :
   else
      # allow to repeat full backup by deleting ${today_dir}
      [ ${debug} = true ] && msg DEBUG is enabled. you need to manually remove "${today_dir}" before repeating || rm -rf "${today_dir}"
   fi
   if [ ${debug} = false ]
   then
      rm -rf "${tmpdir}"
   fi
else
   # create directory to keep the backup
   mkdir ${incr_dir} || error directory ${incr_dir} can\'t be created
   bkupdir="${incr_dir}"
   # create temporary directory to store files
   tmpdir="$(mktemp -d ${TMP:-"${basedir}"/tmp}/backupincr.XXXXXXXX)"
   chmod g+rwxs "${tmpdir}"
   # evaluate the path name of todays full backup
   full_dir="$(ls -d "${today_dir}"/full-*)"
   # create the incremental backup script
   create_incr_script
   # execute the script
   msg executing "${tmpdir}/incr_backup.sh"
   cat "${tmpdir}/incr_backup.sh"
   "${tmpdir}/incr_backup.sh" 2>&1 | tee "${incr_dir}/incr_backup.log"
   RC=${PIPESTATUS[0]}
   msg Returncode of "${tmpdir}/incr_backup.sh" was ${RC}
   if [ "${RC}" = 0 ]
   then 
      :
   else
      # delete the directory if the incremental backup failed
      [ ${debug} = true ] && msg DEBUG is enabled. you need to manually remove "${incr_dir}" to avoid confusion || rm -rf "${today_dir}"
   fi
   if [ ${debug} = false ]
   then
      rm -rf "${tmpdir}"
   fi
fi

