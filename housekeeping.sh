#!/bin/bash

# establish base directory for the scripts
basedir=$( dirname "$( readlink -e "$0" )" )
toolname="$( basename "$0" )"
cd "${basedir}"

# Print usage information
usage() {
cat <<EOF
Usage: ${toolname} [--config | -c configfile] 
                  [ -d | --debug ]
          --config configfile is located in ${basedir}/etc/config.sh by default
             must be an absolute path or relative to ${basedir}
          --debug enable debug mode, temporary files will not be deleted.

${toolname} deletes all outdated directories in
        - /tmp
        - ${basedir}/tmp
        - <backupdir> (see your configfile)
ENSURE NO OTHER OF THESE SCRIPTS IS CURRENTLY RUNNING

EOF
exit 2
}

# set default config file
config="${basedir}/etc/config.sh"
debug=false

# parse arguments
SHORT=c:,h,d
LONG=config:,help,debug
OPTS=$( getopt -a -n "${toolname}" --options ${SHORT} --longoptions ${LONG} -- "$@" )
[ $? -ne 0 ] && usage
eval set -- "${OPTS}"
while :
do
   case "$1" in
      -c | --config )
         config="$2"
         shift 2
         ;;
      -d | --debug )
         debug=true
         shift 1
         ;;
      -h | --help)
         usage
         exit 2
         ;;
      --)
         shift;
         break
         ;;
      *)
         echo "Unexpected option: $1"
         usage
         ;;
   esac
done

if [ ! -e "${config}" -a ! -e "${basedir}/${config}" ]
then
   echo configfile ${config} not foud
   usage
fi

. "${basedir}"/bin/functions.sh
check_config
aquire_lock

# check required parameters

msg Starting housekeeping using configfile ${config}

msg deleting obsolete full backups
fullbackups=$( ls -d "${backupdir}"/2???-??-?? | head --lines=-${days_of_backups} )
for i in ${fullbackups}
do
   rm -rf "$i"
done

msg deleting temporary files
Temp="$(ls -d "${TMP:-"${basedir}"}/tmp"/{backupfull,backupincr,restore,prepare}.???????? 2>/dev/null )"
for i in ${Temp}
do
   rm -rf "$i"
done

