#!/bin/bash
cat <<EOF
To be prepared to use backup/prepare/restore/housekeeping scripts
you need to perform the following installation tasks manually:

0. download and unpack the package 
   - wget https://gitlab.com/onkelhartwig/build-m4b-audio-book-from-mp3-files/-/archive/master/build-m4b-audio-book-from-mp3-files-master.tar.gz
   - tar -xzf build-m4b-audio-book-from-mp3-files-master.tar.gz
   - cd build-m4b-audio-book-from-mp3-files-master
1. create a unix group to be primary group for the tasks
   - groupadd backup
2. create an user responsible to run the scripts
   - useradd -g backup -G mysql -s /bin/bash backup
3. create an MariaDB user to interact with mariabackup
   - mysql -u root
   - CREATE USER 'backup'@'localhost' IDENTIFIED BY 'password';
   - GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT, CREATE TABLESPACE, PROCESS, SUPER, CREATE, INSERT, SELECT ON *.* TO 'backup'@'localhost';
   - FLUSH PRIVILEGES;
   - SELECT @@datadir; /* Remember the datadir, default is /var/lib/mysql */
4. add the group backup as supplementary group for user mysql
   - usermod -G backup mysql
5. the unix user backup will need read access to all directories below @@datadir (/var/lib/mysql)
   - find /var/lib/mysql -type d -exec chmod 750 {} \;
6. create an mysql extra configuration file /etc/mysql/backup.cnf with the following content
   [client]
   user=backup
   password=password
7. establish security on this file
   - chown backup /etc/mysql/backup.cnf
   - chmod 600 /etc/mysql/backup.cnf
8. create a directory to contain the backups
   - mkdir -p /data/backups/mariadb
   - chown backup:mysql /data/backups/mariadb
   - chmod g+rwxs /data/backups/mariadb
9. carefully update the build-m4b-audio-book-from-mp3-files-master/etc/config.sh to fit your needs
10. carefully read the README.md and do a test with your MariaDB databases. As always, have a working backup available

ToDo's:
   - implement cascading incremental backups - near future
   - implement binlog processing - future feature
   - implement compression of backed up files - near future
